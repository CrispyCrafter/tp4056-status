from machine import Pin
class TP4056:
    def __init__(self, pins):
        self.chrg = Pin(pins[0], mode=Pin.IN, pull=Pin.PULL_UP)
        self.stdby = Pin(pins[1], mode=Pin.IN, pull=Pin.PULL_UP)
    
    def status(self):
        if not self.chrg():
            return 0
        elif not self.stdby():
            return 1
        else:
            return None


